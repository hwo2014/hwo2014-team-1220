module Stephan.Stephan where

import Data.Maybe
import Data.List
import Debug.Trace

import Messaging.ClientMessage
import Messaging.ServerMessage
import State.State(BotState(..))
import Predicate.Predicate

import Util

import qualified Messaging.CarPositionsMessageData as CPMD
import qualified Messaging.GameInitMessageData as GIMD
import qualified Messaging.JoinedMessageData as JMD
import qualified Model.Position as Position
import qualified Model.Piece as Piece
import qualified Model.Car as Car




reportTrackPieces :: [Piece.Piece] -> String
reportTrackPieces pieces = unwords ["Pieces:", p] where
    p = show pieces

reportPiecePosition :: CPMD.CarPositionsData -> String
reportPiecePosition carPosition = unwords["Car:", index] where
    index = show $ piecePositionIndex carPosition

piecePositionIndex :: CPMD.CarPositionsData -> Int
piecePositionIndex = Position.pieceIndex . CPMD.position

reportGameInit :: GIMD.GameInitData -> String
reportGameInit gameInit = unwords ["Players:", playerList, ",", "Track:", trackLength, "pieces"] where
    playerList = show $ players gameInit
    trackLength = show (length $ GIMD.pieces gameInit) 
    players = map Car.carId . GIMD.cars

handle :: ServerMessage -> BotState -> IO (BotState, ClientMessage)
handle message previousState = do
    let newState = previousState 
    case messageData message of
        Joined jmd              -> return $ handleJoined jmd newState
        GameInit gimd           -> return $ handleGameInit gimd newState { gimd = gimd }
        CarPositions cpmd       -> do
            putStrLn $ show $ GIMD.segments $ gimd newState
            return $ handleCarPositions cpmd newState { cpmd = cpmd }
        GameStart               -> return (newState, Ping)
        GameEnd _               -> do
            putStrLn "Game ended"
            return (previousState, Ping)
        LapFinished _           -> return (newState, Ping)
        TurboAvailable tamd     -> do
            putStrLn "Turbo Available"
            return (newState { tamd = Just tamd }, Ping)
        Unknown msgType msgData -> do
            putStrLn $ "Unknown message: type: " ++ msgType ++ ", data: " ++ msgData
            return (previousState, Ping)
        _                       -> return (newState, Ping)

handleJoined :: JMD.JoinedData -> BotState -> Action
handleJoined jmd state = (state { botName = JMD.name jmd }, Ping)

decideAction :: BotState -> [(String, Bool)] -> Action
decideAction state predicates =
    if doesApply "onCurve" predicates
        then (state, Throttle 0.4)
        else
    if doesApply "isTurboStraight" predicates
        then (state, Throttle 1.0)
        else
    if doesApply "inLastLapAndOnFinishStraight" predicates
        then (state, Ping)
        else 
    if doesApply "onSwitch" predicates
        then (state, Switch $ findDirection state)
        else
    if doesApply "onFollowingCurveAndReadyForBreak" predicates
        then (state, Throttle 0.4) 
        else (state, Throttle 0.7) where
    findDirection state = Piece.direction nextCurve where 
        nextCurve = if null allCurves then nextnext else head allCurves -- irgendwann ist die liste leer, in nächster runde, also von vorne gucken -> nextnext 
        nextSwitch = head allSwitches
        allCurves = drop (currentPieceIndex-1) $ filter (\x -> Piece.curve x == True) $ GIMD.pieces $ gimd state
        allSwitches = drop (currentPieceIndex-1) $ filter (\x -> Piece.switch x == True) $ GIMD.pieces $ gimd state
        currentPieceIndex = CPMD.pieceIndex $ CPMD.stephan $ cpmd state
        nextnext = head $ filter (\x -> Piece.curve x == True) $ GIMD.pieces $ gimd state -- nextnext guckt wieder von vorne (neue Runde)

doesApply :: Name -> [(Name, Bool)] -> Bool
doesApply name predicates = case find (\x -> fst x == name) predicates of
    Nothing -> False
    Just (_,b) -> b
   
prioritize :: [(String, Bool)] -> (String, Bool)
prioritize = head

myCombinations :: [Combination2]
myCombinations = [inLastLapAndOnFinishStraight, onSwitchAndIsCurveBetweenSwitches, onFollowingCurveAndReadyForBreak]

inLastLapAndOnFinishStraight :: Combination2
inLastLapAndOnFinishStraight predicates = ("inLastLapAndOnFinishStraight", result) where
    result = isGiven IsInLastLap predicates && isGiven OnFinishStraight predicates

onSwitchAndIsCurveBetweenSwitches :: Combination2
onSwitchAndIsCurveBetweenSwitches predicates = ("onSwitch", result) where
    result = isGiven IsBeforeSwitch predicates && isGiven IsCurveBetweenSwitches predicates

onFollowingCurveAndReadyForBreak :: Combination2
onFollowingCurveAndReadyForBreak predicates = ("onFollowingCurveAndReadyForBreak", result) where
    result = isGiven OnFollowingCurve predicates && isGiven ReadyForBreak predicates

onCurve :: Combination2
onCurve predicates = ("onCurve", result) where
    result = isGiven OnCurve predicates

isTurboStraight :: Combination2
isTurboStraight predicates = ("isTurboStraight", result) where
    result = isGiven IsTurboStraight predicates
    
handleGameInit :: GIMD.GameInitData -> BotState -> Action
handleGameInit gimd state = (state { gimd=gimd }, Ping)

handleCarPositions :: [CPMD.CarPositionsData] -> BotState -> Action
handleCarPositions cpmd state = decideAction state . aggregatePredicates myCombinations . checkState $ updateState state where
    updateState :: BotState -> BotState
    updateState state = state { cpmd = cpmd } 