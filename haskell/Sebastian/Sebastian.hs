module Sebastian.Sebastian where

import           Messaging.ClientMessage
import           Messaging.ServerMessage             as Message
import qualified Messaging.TurboAvailableMessageData as Payload.TAM
import qualified Model.CarId                         as Data.Model.Car
import qualified Sebastian.Model.Car                 as Model.Car
import qualified Sebastian.Model.Model               as Model
import           State.State
import           Util

--{-# ANN module "HLint: ignore" #-}

handle :: ServerMessage -> BotState -> IO (BotState, ClientMessage)
handle message botState = case botState of
--    SeState state -> do
--        response <- handle2 message state
--        return (SeState (fst response), (snd response))
    _ -> do
--        response <- handle2 message def
--        return (SeState (fst response), (snd response))
        return (botState, Ping)

--handle2 :: ServerMessage -> SebastiansBotState -> IO (SebastiansBotState, ClientMessage)
--handle2 message state = do
--    putStrLn "Test"
--    return (state, Ping)
--
--
--control :: SebastiansBotState -> ServerMessage -> ClientMessage
--control state message = case messageData message of
--    Joined _ ->
--        Ping
--    YourCar payload ->
--        Ping
--    GameInit payload ->
--        Ping
--    GameStart ->
--        policy state
--    CarPositions _ ->
--        policy state
--    TurboAvailable _ ->
--        policy state
--    Crash _ ->
--        policy state
--    Spawn _ ->
--        policy state
--    Disqualified _ ->
--        policy state
--    LapFinished _ ->
--        policy state
--    Finish _ ->
--        policy state
--    GameEnd payload ->
--        Ping
--    TournamentEnd ->
--        Ping
--    _ ->
--        Ping
--
--policy :: SebastiansBotState -> ClientMessage
--policy state = Throttle 0.64
--
--updateStateBefore :: SebastiansBotState -> ServerMessage -> SebastiansBotState
--updateStateBefore state message = case (messageData message) of
--    Joined _ ->
--        state
--    YourCar payload ->
--        state {environment = env} where
--            env = (environment state) {Model.car = car} where
--                car = (Model.car (environment state)) {Model.Car.id = id} where
--                    id = Model.Car.Identifier (Data.Model.Car.name payload) (Data.Model.Car.color payload)
----    GameInit payload ->
----        state {environment = env} where
----            env = (environment state) {Model.track = track, Model.car = car, Model.race = race} where
----                track = Model.Track.Track (Model.trackShape pieces) (Model.trackSwitches pieces) (Model.positionOfPiece pieces) (Model.distanceFromCenterOfLane lanes) (Model.trackLength pieces) (length lanes) where
----                    pieces = Data.GameInit.piecesOfGame payload
----                    lanes = Data.GameInit.lanesOfGame payload
----                car = (Model.car (environment state)) {Model.Car.dimensions = dimensions} where
----                    dimensions = Model.Car.Dimensions length width origin where
----                        length = maybe (Model.Car.length (Model.Car.dimensions (Model.car (environment state)))) (\x -> (float2Double (Data.Car.dimensionLength (Data.Car.dimensions x)))) (find (\x -> (Data.Car.carIdName (Data.Car.carId x)) == (Model.Car.name (Model.Car.id (Model.car (environment state))))) (Data.GameInit.cars (Data.GameInit.race payload)))
----                        width = maybe (Model.Car.width (Model.Car.dimensions (Model.car (environment state)))) (\x -> (float2Double (Data.Car.width (Data.Car.dimensions x)))) (find (\x -> (Data.Car.carIdName (Data.Car.carId x)) == (Model.Car.name (Model.Car.id (Model.car (environment state))))) (Data.GameInit.cars (Data.GameInit.race payload)))
----                        origin = maybe (Model.Car.origin (Model.Car.dimensions (Model.car (environment state)))) (\x -> (float2Double (Data.Car.guideFlagPosition (Data.Car.dimensions x)))) (find (\x -> (Data.Car.carIdName (Data.Car.carId x)) == (Model.Car.name (Model.Car.id (Model.car (environment state))))) (Data.GameInit.cars (Data.GameInit.race payload)))
----                race = Model.Race.Race laps maxLapTime where
----                    laps = maybe (Model.Race.laps (Model.race (environment state))) (\x->x) (Data.GameInit.laps (Data.GameInit.raceSession (Data.GameInit.race payload)))
----                    maxLapTime = maybe (Model.Race.maxLapTime (Model.race (environment state))) (\x->x) (Data.GameInit.maxLapTimeMs (Data.GameInit.raceSession (Data.GameInit.race payload)))
--    GameStart ->
--        state
----    CarPositions payload ->
----        state {environment = env} where
----            env = (environment state) {Model.tick = tick, Model.car = car} where
----                tick = maybe (Model.tick (environment state)) (\x -> x) (Message.tick message)
----                car = (Model.car (environment state)) {Model.Car.position = position, Model.Car.driftAngle = angle, Model.Car.blinker = blinker} where
----                    position = (Model.Car.position (Model.car (environment state))) {Model.Car.lap = lap, Model.Car.offset = offset, Model.Car.lane = lane} where
----                        mypos = Data.CPM.findCar (Model.Car.name (Model.Car.id (Model.car (environment state)))) payload
----                        lap = maybe (Model.Car.lap (Model.Car.position (Model.car (environment state)))) (\x -> (Data.CPM.lap (Data.CPM.piecePosition x))) mypos
----                        offset = maybe (Model.Car.offset (Model.Car.position (Model.car (environment state)))) (\x -> (Model.Track.positionOfPiece (Model.track (environment state))) (Data.CPM.pieceIndex (Data.CPM.piecePosition x)) (float2Double (Data.CPM.inPieceDistance (Data.CPM.piecePosition x)))) mypos
----                        lane = maybe (Model.Car.lane (Model.Car.position (Model.car (environment state)))) (\x -> (Model.Track.distanceFromCenterOfLane (Model.track (environment state))) (Data.CPM.startLaneIndex (Data.CPM.lane (Data.CPM.piecePosition x))) (Data.CPM.endLaneIndex (Data.CPM.lane (Data.CPM.piecePosition x)))) mypos
----                    angle = maybe (Model.Car.driftAngle (Model.car (environment state))) (\x -> float2Double (Data.CPM.angle x)) (Data.CPM.findCar (Model.Car.name (Model.Car.id (Model.car (environment state)))) payload)
----                    blinker | deactivateBlinker (Model.Car.position (Model.car (environment state))) position = Model.Car.Off
----                            | otherwise = (Model.Car.blinker (Model.car (environment state))) where
----                                deactivateBlinker :: Model.Car.Position -> Model.Car.Position -> Bool
----                                deactivateBlinker previousPos currentPos = ((Model.Track.switches (Model.track (environment state))) (Model.Car.offset previousPos)) < ((Model.Track.switches (Model.track (environment state))) (Model.Car.offset currentPos))
--    TurboAvailable payload ->
--        state {environment = env} where
--            env = (environment state) {Model.car = car} where
--                car = (Model.car (environment state)) {Model.Car.turbo = turbo} where
--                    turbo = Model.Car.Turbo (Payload.TAM.turboFactor payload)  (Payload.TAM.turboDurationTicks payload)
--    Crash payload | (Data.Model.Car.name payload) == (Model.Car.name (Model.Car.id (Model.car (environment state)))) -> state {environment = env }
--                  | otherwise -> state where
--                        env = (environment state) {Model.car = car} where
--                            car = (Model.car (environment state)) {Model.Car.position = position} where
--                                position = (Model.Car.position (Model.car (environment state))) {Model.Car.onTrack = False}
--    Spawn payload | (Data.Model.Car.name payload) == (Model.Car.name (Model.Car.id (Model.car (environment state)))) -> state {environment = env }
--                  | otherwise -> state where
--                        env = (environment state) {Model.car = car} where
--                            car = (Model.car (environment state)) {Model.Car.position = position} where
--                                position = (Model.Car.position (Model.car (environment state))) {Model.Car.onTrack = True}
--    Disqualified payload ->
--        state
--    LapFinished payload ->
--        state
--    Finish payload ->
--        state
--    GameEnd payload ->
--        state
--    TournamentEnd ->
--        state
--    _ ->
--        state
--
--updateStateAfter :: SebastiansBotState -> ClientMessage -> SebastiansBotState
--updateStateAfter state message = case message of
--    Throttle value ->
--        state {environment = env} where
--             env = (environment state) {Model.car = car} where
--                 car = (Model.car (environment state)) {Model.Car.throttle = value}
--    Turbo ->
--        state {environment = env} where
--            env = (environment state) {Model.car = car} where
--                car = (Model.car (environment state)) {Model.Car.turbo = turbo, Model.Car.boost = boost} where
--                    turbo = Model.Car.Turbo 0.0 0
--                    boost = Model.Car.turbo (Model.car (environment state))
--    Switch direction -> case direction of
--        ToLeft ->
--            state {environment = env} where
--                env = (environment state) {Model.car = car} where
--                    car = (Model.car (environment state)) {Model.Car.blinker = Model.Car.Left}
--        ToRight ->
--            state {environment = env} where
--                env = (environment state) {Model.car = car} where
--                    car = (Model.car (environment state)) {Model.Car.blinker = Model.Car.Right}
--    _ -> state
--
