module Sebastian.Model.Utils where

import           Data.List

import qualified Model.Lane as Lane
import qualified Model.Piece as Piece

{-# ANN module "HLint: ignore" #-}

distanceFromCenterOfLane :: [Lane.Lane] -> (Int -> Int -> Float)
distanceFromCenterOfLane lanes = f where
    f :: Int -> Int -> Float
    f i j = (a + b) / 2.0 where
        a = fromIntegral (maybe 0 (\x -> (Lane.centerDistance x)) (find (\x -> (Lane.index x) == i) lanes))
        b = fromIntegral (maybe 0 (\x -> (Lane.centerDistance x)) (find (\x -> (Lane.index x) == j) lanes))

positionOfPiece :: [Piece.Piece] -> (Int -> Float -> Float)
positionOfPiece pieces = f
    where
        f :: Int -> Float -> Float
        f i offset = ((trackLength (take i pieces)) + offset) / length
            where
                length = trackLength pieces


-- creates the track shape function
-- trackShape: [0.0..1.0] -> [-1.0..1.0]
trackShape :: [Piece.Piece] -> (Float -> Float)
trackShape pieces = f
    where
        f :: Float -> Float
        f t | Piece.straight piece = 0.0
            | Piece.curve piece = (signum (Piece.angle piece)) * 100.0 / (fromIntegral $ Piece.radius piece)
            | otherwise = 0.0
            where
                piece = pieceAtPos pieces (t * (trackLength pieces))

-- creates the track switches function
-- trackShape: [0.0..1.0] -> {0,1}
trackSwitches :: [Piece.Piece] -> (Float -> Float)
trackSwitches pieces = f
    where
        f :: Float -> Float
        f t | Piece.switch piece = 1.0
            | otherwise = 0.0
            where
                piece :: Piece.Piece
                piece = pieceAtPos pieces (t * (trackLength pieces))

-- calculates the track length
trackLength :: [Piece.Piece] -> Float
trackLength pieces = sum (map pieceLength pieces)

-- utility functions
pieceAtPos :: [Piece.Piece] -> Float -> Piece.Piece
pieceAtPos pieces x = f pieces 0.0 where
    f :: [Piece.Piece] -> Float -> Piece.Piece
    f ps y | y + (pieceLength (head ps)) < x = f (tail ps) (y + (pieceLength (head ps)))
           | otherwise = (head ps)

pieceLength :: Piece.Piece -> Float
pieceLength piece | Piece.straight piece = Piece.length piece
                  | Piece.curve piece = curveLength piece
                  | otherwise = 0.0

curveLength :: Piece.Piece -> Float
curveLength piece = pi * (fromIntegral $ Piece.radius piece) * (abs (Piece.angle piece)) / 180.0

