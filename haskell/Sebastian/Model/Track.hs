module Sebastian.Model.Track where

import Util

data Track = Track {
    shape                    :: Double -> Double,
    switches                 :: Double -> Double,
    positionOfPiece          :: Int -> Double -> Double,
    distanceFromCenterOfLane :: Int -> Int -> Double,
    length                   :: Double,
    lanes                    :: Int
}

instance Default Track where
    def = Track (const 0.0) (const 0.0) (\i x -> 0.0) (\i j -> 0.0) 0.0 0