module Sebastian.Model.Race where

import Util

data Race = Race {
    laps       :: Int,
    maxLapTime :: Int
}

instance Default Race where
    def = Race 0 0