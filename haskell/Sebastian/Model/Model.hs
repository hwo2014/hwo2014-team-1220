module Sebastian.Model.Model where

    
import Util
    
import qualified Sebastian.Model.Car   as Model.Car
import qualified Sebastian.Model.Race  as Model.Race
import qualified Sebastian.Model.Track as Model.Track


data Model = Model {
    tick  :: Int,
    track :: Model.Track.Track,
    car   :: Model.Car.Car,
    race  :: Model.Race.Race
}

instance Default Model where
    def = Model 0 def def def