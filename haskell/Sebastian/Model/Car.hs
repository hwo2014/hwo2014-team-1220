module Sebastian.Model.Car where

import Util

data Car = Car {
    id :: Identifier,
    dimensions :: Dimensions,
    position :: Position,
    driftAngle :: Double,
    throttle :: Double,
    blinker :: Blinker,
    turbo :: Turbo,
    boost :: Turbo
}

instance Default Car where
    def = Car def def def 0.0 0.0 Off (Turbo 0.0 0) (Turbo 0.0 0)

data Turbo = Turbo Double Int

data Blinker = Left | Off | Right

data Identifier = Identifier {
    name  :: String,
    color :: String
}

instance Default Identifier where
    def = Identifier "" ""

data Dimensions = Dimensions {
    length :: Double,
    width :: Double,
    origin :: Double
}

instance Default Dimensions where
    def = Dimensions 0.0 0.0 0.0

data Position = Position {
    lap :: Int,
    offset :: Double,
    lane :: Double,
    onTrack :: Bool
}

instance Default Position where
    def = Position 0 0.0 0.0 True
