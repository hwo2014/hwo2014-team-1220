module DefaultHandler where

import Messaging.ClientMessage
import Messaging.ServerMessage
import State.State
import Predicate.Predicate

import qualified Messaging.JoinedMessageData as JMD


handle :: ServerMessage -> BotState -> IO (BotState, ClientMessage)
handle message state = case messageData message of
    Joined jmd -> return $ handleJoined jmd state
    GameInit _ -> return (state, Ping)
    CarPositions _ -> return (state, Throttle 0.6)
    Unknown msgType _ -> do
        putStrLn $ "Unknown message: " ++ msgType
        return (state, Ping)
    _ -> return (state, Ping)

handleJoined :: JMD.JoinedData -> BotState -> Action
handleJoined jmd state = (state { botName = JMD.name jmd }, Ping)
