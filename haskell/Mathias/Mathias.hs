module Mathias.Mathias where


import Data.Maybe
import Debug.Trace
import Messaging.ClientMessage
import Messaging.ServerMessage
import State.State
import Mathias.MathiasState
import Mathias.MathiasCombinations
import Predicate.Predicate

import Util

import qualified Messaging.GameInitMessageData as GIMD
import qualified Messaging.CarPositionsMessageData as CPMD
import qualified Messaging.JoinedMessageData as JMD
import qualified Messaging.TurboAvailableMessageData as TAMD
import qualified Messaging.TurboStartMessageData as TSMD
import qualified Messaging.TurboEndMessageData as TEMD


handle :: ServerMessage -> BotState -> IO (BotState, ClientMessage)
handle message previousState = do
    let newState = addMessageToLog message previousState
    case messageData message of
        Joined jmd              -> do
            putStrLn $ "Joined bot Name: " ++ JMD.name jmd
            return $ handleJoined jmd newState
        GameInit newGimd        -> --do
            return $ handleGameInit newGimd newState
        CarPositions newCpmd    -> do
            let cpmdUpdate = newState { cpmd = newCpmd }
            let evenNewerState = addPredicatesAndCombinationsToLog cpmdUpdate
            let msg = handleCarPositions newCpmd evenNewerState
            return msg
        TurboStart tsmd         -> return $ handleTurboStart tsmd newState
        TurboEnd temd           -> return $ handleTurboEnd temd newState
        TurboAvailable newTamd  -> return $ handleTurboAvailable newTamd newState
        GameStart               -> return (newState, Ping)
        GameEnd _               -> do
            putStrLn "Game ended"
            return (previousState, Ping)
        LapFinished _           -> return (newState, Ping)
        Unknown msgType msgData -> do
            putStrLn $ "Unknown message: type: " ++ msgType ++ ", data: " ++ msgData
            return (previousState, Ping)
        _                       -> return (newState, Ping)
        where
            addMessageToLog msg state = state { individualState = newIndividualState } where
                newIndividualState = MaState . addMessage msg $ getIndividualState state
            addPredicatesAndCombinationsToLog state = 
                state { individualState = newIndividualState $ getIndividualState state } where
                    newIndividualState = MaState . addPredicatesAndCombinations index (map show ps) cs
                    ps = getGivenPredicates state
                    cs = getApplyingCombinations myCombinations ps
                    index = fromMaybe 0 $ tick message

getIndividualState :: BotState -> MathiasState
getIndividualState = myState . individualState where
    myState (MaState state) = state
    myState _ = def 
            
decide :: BotState -> [Name] -> Action
decide state combinations 
    | applies isBeforeSwitchAndNextCurveIsLeftCurve = trace "Left" (state, Switch ToLeft)
    | applies isBeforeSwitchAndNextCurveIsRightCurve = trace "Right" (state, Switch ToRight)
    | applies inCurveSegment = trace (show combinations) (state, Throttle 0.4)
    | applies onStraightSegment = trace (show combinations) (state, Throttle 0.6)
    | otherwise = (state, Ping) 
    where
        applies = combinationApplies combinations
--        allApply = allCombinationsApply combinations
--        anyApplies = anyCombinationApplies combinations

handleJoined :: JMD.JoinedData -> BotState -> Action
handleJoined jmd state = trace ("New Name: " ++ JMD.name jmd) (state { botName = JMD.name jmd }, Ping)

handleGameInit :: GIMD.GameInitData -> BotState -> Action
handleGameInit newGimd state = (state { gimd = newGimd, botName = setBotName }, Ping) where
    setBotName = if null $ botName state then "CaaS_Mathias" else botName state

handleCarPositions :: [CPMD.CarPositionsData] -> BotState -> Action
handleCarPositions newCpmd state = decide newState $ combos givenPredicates where
    givenPredicates = getGivenPredicates newState
    combos = getApplyingCombinations myCombinations
    newState = state { cpmd = newCpmd }

handleTurboAvailable :: TAMD.TurboAvailableData -> BotState -> Action
handleTurboAvailable newTamd state = (state { tamd = Just newTamd }, Ping)

handleTurboStart :: TSMD.TurboStartData -> BotState -> Action
handleTurboStart tsmd state = (newState, Ping) where
    newState = if isOwnCar then state { tamd = Nothing } else state where
        isOwnCar = ownCar (TSMD.name tsmd) state

handleTurboEnd :: TEMD.TurboEndData -> BotState -> Action
handleTurboEnd _ state = (state, Ping)



addPredicatesAndCombinations :: Int -> Names -> Names -> MathiasState -> MathiasState
addPredicatesAndCombinations index ps cs state = state { 
    predicateLog = (index, ps) : predicateLog state,
    combinationLog = (index, cs) : combinationLog state  
}


pieceOffset :: Float
pieceOffset = 100.0




