module Mathias.MathiasState where


import Util

import Messaging.ServerMessage


data MathiasState = MathiasState {
    messageLog :: [ServerMessage],
    predicateLog :: [(Int, Names)],
    combinationLog :: [(Int, Names)]
}

instance Default MathiasState where
    def = MathiasState def def def

addMessage :: ServerMessage -> MathiasState -> MathiasState
addMessage message state = state { messageLog = message : messageLog state }
