module Mathias.MathiasCombinations where


import Predicate.Predicate
import Util


myCombinations :: Combinations
myCombinations = 
    [
        inCurveSegmentCombination, 
        onStraightSegmentCombination,
        isBeforeSwitchAndNextCurveIsLeftCurveCombination,
        isBeforeSwitchAndNextCurveIsRightCurveCombination
    ]


inLastLapAndOnFinishStraightCombination :: Combination
inLastLapAndOnFinishStraightCombination = (inLastLapAndOnFinishStraight, result) where
    result = allPredicatesApply [IsInLastLap, OnFinishStraight]

inLastLapAndOnFinishStraight :: Name
inLastLapAndOnFinishStraight = "inLastLapAndOnFinishStraight"

inCurveSegment :: Name
inCurveSegment = "inCurveSegment"

inCurveSegmentCombination :: Combination
inCurveSegmentCombination = (inCurveSegment, predicateApplies OnCurve)

onStraightSegment :: Name
onStraightSegment = "onStraightSegment"

onStraightSegmentCombination :: Combination
onStraightSegmentCombination = (onStraightSegment, predicateApplies OnStraight)

isBeforeSwitchAndNextCurveIsLeftCurve :: Name
isBeforeSwitchAndNextCurveIsLeftCurve = "isBeforeSwitchAndNextCurveIsLeftCurve"

isBeforeSwitchAndNextCurveIsLeftCurveCombination :: Combination
isBeforeSwitchAndNextCurveIsLeftCurveCombination = 
    (isBeforeSwitchAndNextCurveIsLeftCurve, allPredicatesApply [IsBeforeSwitch, NextCurveIsLeftCurve])
    
isBeforeSwitchAndNextCurveIsRightCurve :: Name
isBeforeSwitchAndNextCurveIsRightCurve = "isBeforeSwitchAndNextCurveIsRightCurve"

isBeforeSwitchAndNextCurveIsRightCurveCombination :: Combination
isBeforeSwitchAndNextCurveIsRightCurveCombination = 
    (isBeforeSwitchAndNextCurveIsRightCurve, allPredicatesApply [IsBeforeSwitch, NextCurveIsRightCurve])