{-# LANGUAGE OverloadedStrings #-}

module Main where

import           System.Environment (getArgs)
import           System.Exit (exitFailure)

import           Control.Exception(throw, PatternMatchFail (..))
import           Data.Aeson (Result (..), decode, fromJSON)
import qualified Data.ByteString.Lazy.Char8 as L
import           Network (PortID (..), connectTo)
import           System.IO (BufferMode (..), Handle, hGetLine, hPrint, hSetBuffering, hIsEOF)

import           Messaging.ServerMessage
import           Messaging.ClientMessage
import           State.State
import           Util

import qualified Stephan.Stephan as Stephan
import qualified Sebastian.Sebastian as Sebastian
import qualified Mathias.Mathias as Mathias
import qualified DefaultHandler


testRaceTrack :: String
testRaceTrack = "keimola"

main :: IO ()
main = do
    args <- getArgs
    case args of
        [server, handler]               -> runTestRace server $ read handler
        [server, port, botname, botkey] -> runCiRace server (read port) botname botkey Mathias where
        _ -> do
            putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> or hwo2014bot <host> <implementation>"
            exitFailure

runCiRace :: String -> Integer -> String -> String -> Bot -> IO ()
runCiRace server port botname botkey handler = do
    handle <- connectToServer server port
    putStrLn $ unwords ["Connected to ci server:", server]
    hSetBuffering handle LineBuffering
    let message = Join botname botkey
    hPrint handle message
    putStrLn "Join message send"
    handleMessages handle (getHandler handler) def

runTestRace :: String -> Bot -> IO ()
runTestRace server handler = do
    handle <- connectToServer server 8091
    putStrLn $ unwords ["Connected to test server:", server]
    hSetBuffering handle LineBuffering
    let message = JoinRace botname "2UidMax74f36oQ" testRaceTrack "" 1 where
        botname = "CaaS_" ++ show handler
    hPrint handle message
    handleMessages handle (getHandler handler) def
    
connectToServer :: String -> Integer -> IO Handle
connectToServer server = connectTo server . PortNumber . fromIntegral

handleMessages :: Handle -> MessageHandler -> BotState -> IO ()
handleMessages h handler state = do
    ineof <- hIsEOF h
    if ineof 
        then return ()
        else decodeMessage h handler state

decodeMessage :: Handle -> MessageHandler -> BotState -> IO ()
decodeMessage handle handler state = do
    result <- hGetLine handle
    case decode (L.pack result) of
        Just json -> do
            let decoded = fromJSON json
            case decoded of
                Success message -> handleServerMessage handle handler state $ toMessage message
                Error s -> do
                    throw $ PatternMatchFail ("Error decoding message: " ++ s ++ "(" ++ result ++ ")")
        Nothing -> throw $ PatternMatchFail ("Error parsing JSON: " ++ show result)

handleServerMessage :: Handle -> MessageHandler -> BotState -> ServerMessage -> IO ()
handleServerMessage handle messageHandler state serverMessage = do
    response <- messageHandler serverMessage state
    mapM_ (hPrint handle) [snd response]
    handleMessages handle messageHandler $ fst response

type MessageHandler = ServerMessage -> BotState -> IO (BotState, ClientMessage)

data Bot = 
    Mathias 
  | Sebastian 
  | Stephan 
  | Default 
  deriving(Read,Show)

getHandler :: Bot -> ServerMessage -> BotState -> IO (BotState, ClientMessage)
getHandler Mathias = Mathias.handle
getHandler Stephan = Stephan.handle
getHandler Sebastian = Sebastian.handle
getHandler Default = DefaultHandler.handle


