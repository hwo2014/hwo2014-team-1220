module Predicate.Predicate where


import Data.List
import Data.Maybe

import Util

import Messaging.ClientMessage(ClientMessage())
import qualified Messaging.GameInitMessageData as GIMD
import qualified Messaging.CarPositionsMessageData as CPMD
import qualified Model.Piece as Piece
import qualified Model.Segment as Segment
import qualified State.State as State
import qualified Model.Track as Track


type Action = (State.BotState, ClientMessage)

type Combination = (Name, CombinationFunction)

type Combinations = [Combination]

type CombinationFunction = Predicates -> Bool


getApplyingCombinations :: Combinations -> Predicates -> [Name]
getApplyingCombinations combinations predicates = map fst $ filter checkCombo combinations where
    checkCombo combination = snd combination predicates 

combinationApplies :: Names -> Name -> Bool
combinationApplies = flip elem 

allCombinationsApply :: Names -> Names -> Bool
allCombinationsApply = all . combinationApplies

anyCombinationApplies :: Names -> Names -> Bool
anyCombinationApplies = any . combinationApplies


data Predicate = 
    IsInLastLap
  | OnFinishStraight
  | IsTurboStraight
  | IsBeforeSwitch
  | IsCurveBetweenSwitches
  | OnFollowingCurve
  | ReadyForBreak
  | OnCurve
  | OnStraight
  | NextCurveIsLeftCurve
  | NextCurveIsRightCurve
  deriving (Eq, Show)
  
type Predicates = [Predicate]

type PredicateFunction = State.BotState -> Bool


getGivenPredicates :: State.BotState -> Predicates
getGivenPredicates = checkPredicates availablePredicates 

checkPredicates :: [(Predicate, PredicateFunction)] -> State.BotState -> Predicates
checkPredicates ps state = map fst $ filter checkPredicate ps where
    checkPredicate p = snd p state

predicateApplies :: Predicate -> Predicates -> Bool
predicateApplies = elem

allPredicatesApply :: Predicates -> Predicates -> Bool
allPredicatesApply = all . flip predicateApplies

anyPredicateApplies :: Predicates -> Predicates -> Bool
anyPredicateApplies = any . flip predicateApplies

availablePredicates :: [(Predicate, PredicateFunction)]
availablePredicates = 
    [
        (IsInLastLap, isInLastLap),
        (OnFinishStraight, onFinishStraight),
        (IsBeforeSwitch, isBeforeSwitch),
        (IsCurveBetweenSwitches, isCurveBetweenSwitches), 
        (OnFollowingCurve, onFollowingCurve ),
        (ReadyForBreak, readyForBreak),
        (OnCurve, onCurve),
        (OnStraight, onStraight),
        (IsTurboStraight, isTurboStraight),
        (NextCurveIsLeftCurve, nextCurveIsLeftCurve),
        (NextCurveIsRightCurve, nextCurveIsRightCurve)
    ] 

isInLastLap :: State.BotState -> Bool
isInLastLap state = currentLapIndex state + 1 == overallLaps state where
    currentLapIndex = CPMD.lap . CPMD.getCar (State.botName state) . State.cpmd 
    overallLaps = fromMaybe 0 . GIMD.sessionLaps . State.gimd
    
onFinishStraight :: State.BotState -> Bool
onFinishStraight = const False --isOnLastSegment && onStraight

isBeforeSwitch :: State.BotState -> Bool
isBeforeSwitch state = GIMD.pieceSwitch currentPiece (State.gimd state) where
    currentPiece = CPMD.pieceIndex $ CPMD.getCar botName $ State.cpmd state
    botName = State.botName state

isCurveBetweenSwitches :: State.BotState -> Bool
isCurveBetweenSwitches = const False

onFollowingCurve :: State.BotState -> Bool
onFollowingCurve = const False

readyForBreak :: State.BotState -> Bool
readyForBreak = const False

onCurve :: State.BotState -> Bool
onCurve = Segment.curve . State.currentSegment

onStraight :: State.BotState -> Bool
onStraight = Segment.straight . State.currentSegment

isTurboStraight :: State.BotState -> Bool
isTurboStraight = const False

nextCurveIsLeftCurve :: State.BotState -> Bool
nextCurveIsLeftCurve state = getSegmentDirection state == Just ToLeft
    
nextCurveIsRightCurve :: State.BotState -> Bool
nextCurveIsRightCurve state = getSegmentDirection state == Just ToRight
    
getSegmentDirection :: State.BotState -> Maybe Direction
getSegmentDirection state = segmentDirection (nextSegment $ segmentList state) where
    currentSegment = Segment.segmentAt pieceIndex $ segmentList state
    nextSegment = Segment.segmentAt $ mod (Segment.endIndex currentSegment + 1) (Segment.endIndex lastSegment + 1) 
    lastSegment = last $ segmentList state
    pieceIndex = CPMD.pieceIndex . CPMD.getCar (State.botName state) $ State.cpmd state
    segmentDirection segment = case Segment.segmentType segment of
        Segment.Straight -> Nothing
        Segment.Curve _ direction -> Just direction
    segmentList = GIMD.segments . State.gimd

--(IsBeforeSwitch, (GIMD.pieceSwitch pieceIndex $ gimd state) == True),
--(IsCurveBetweenSwitches, True), -- das wir auf einen switch sind, reicht ja nicht und dann in die richtung der nächsten kurve, was wenn noch ein anderer switch folgt ... 
--(OnFollowingCurve, Piece.curve $ GIMD.piece (mod (pieceIndex+1) piecesLength) $ gimd state),
--(ReadyForBreak, Piece.straight $ GIMD.piece (mod (pieceIndex-1) piecesLength) $ gimd state), --Piece.straight $ GIMD.piece (mod pieceIndex piecesLength) $ gimd state   --((GIMD.pieceLength pieceIndex $ gimd state) - (CPMD.inPieceDistance $ CPMD.myCarPositionData $ cpmd state)) > (GIMD.pieceLength pieceIndex $ gimd state) / 1.2
--(OnCurve, Piece.curve $ GIMD.piece (mod (pieceIndex) piecesLength) $ gimd state),
--(IsTurboStraight, True)




type Combination2 = [(Predicate, Bool)] -> (String, Bool)

checkState :: State.BotState -> [(Predicate, Bool)]
checkState state = 
    [
        (IsInLastLap, currentLapIndex == lastLapIndex),
        (OnFinishStraight, True),
        (IsBeforeSwitch, GIMD.pieceSwitch pieceIndex $ State.gimd state),
        (IsCurveBetweenSwitches, True), -- das wir auf einen switch sind, reicht ja nicht und dann in die richtung der nächsten kurve, was wenn noch ein anderer switch folgt ... 
        (OnFollowingCurve, Piece.curve $ GIMD.piece (mod (pieceIndex+1) piecesLength) $ State.gimd state),
        (ReadyForBreak, Piece.straight $ GIMD.piece (mod (pieceIndex-1) piecesLength) $ State.gimd state), --Piece.straight $ GIMD.piece (mod pieceIndex piecesLength) $ gimd state   --((GIMD.pieceLength pieceIndex $ gimd state) - (CPMD.inPieceDistance $ CPMD.myCarPositionData $ cpmd state)) > (GIMD.pieceLength pieceIndex $ gimd state) / 1.2
        (OnCurve, Piece.curve $ GIMD.piece (mod pieceIndex piecesLength) $ State.gimd state),
        (IsTurboStraight, True)
    ] where
        currentLapIndex = CPMD.lap . head $ State.cpmd state
        lastLapIndex = fromMaybe 0 (GIMD.sessionLaps $ State.gimd state) - 1
        pieceIndex = CPMD.pieceIndex $ CPMD.stephan $ State.cpmd state
        piecesLength = length $ GIMD.pieces $ State.gimd state



isGiven :: Predicate -> [(Predicate, Bool)] -> Bool
isGiven predicate predicates = case find (hasPredicate predicate) predicates of
    Nothing -> False
    Just (_, b) -> b 
        
hasPredicate :: Predicate -> (Predicate, Bool) -> Bool
hasPredicate p1 pair = p1 == fst pair 

aggregatePredicates :: [Combination2] -> [(Predicate, Bool)] -> [(String, Bool)]
aggregatePredicates combinations predicates = map execute combinations where
    execute combination = combination predicates
