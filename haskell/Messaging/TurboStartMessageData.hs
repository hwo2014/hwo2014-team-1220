{-# LANGUAGE OverloadedStrings #-}

module Messaging.TurboStartMessageData where


import Util

import qualified Model.CarId as CarId


data TurboStartData = TurboStartData {
    car :: CarId.CarId
}

instance Default TurboStartData where
    def = TurboStartData def
    
name :: TurboStartData -> Name
name = CarId.name . car

color :: TurboStartData -> Color
color = CarId.color . car
