{-# LANGUAGE OverloadedStrings #-}

module Messaging.CrashMessageData where


import Util

import qualified Model.CarId as CarId


type CrashData = CarId.CarId

name :: CrashData -> Name
name = CarId.name

color :: CrashData -> Color
color = CarId.color