{-# LANGUAGE OverloadedStrings #-}

module Messaging.DisqualifiedMessageData where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.CarId as CarId

data DisqualifiedData = DisqualifiedData {
    car :: CarId.CarId,
    reason :: String
}

instance FromJSON DisqualifiedData where
    parseJSON (Object v) =
        DisqualifiedData <$>
        (v .: "car") <*>
        (v .: "reason")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

name :: DisqualifiedData -> Name
name = CarId.name . car

color :: DisqualifiedData -> Color
color = CarId.color . car