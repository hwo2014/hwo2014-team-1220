{-# LANGUAGE OverloadedStrings #-}

module Messaging.JoinedMessageData where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

data JoinedData = JoinedData {
    name :: Name,
    key :: Key
}

instance FromJSON JoinedData where
    parseJSON (Object v) =
        JoinedData <$>
        (v .: "name") <*>
        (v .: "key")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"
