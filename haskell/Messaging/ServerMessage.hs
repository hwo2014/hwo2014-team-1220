{-# LANGUAGE OverloadedStrings #-}

module Messaging.ServerMessage where

import Data.Aeson (FromJSON (..), Result (..), Value (..), fromJSON, parseJSON, (.:), (.:?))
import Control.Applicative
import Control.Exception

import qualified Messaging.JoinedMessageData as JMD
import qualified Messaging.JoinedRaceMessageData as JRMD
import qualified Messaging.YourCarMessageData as YCMD
import qualified Messaging.DisqualifiedMessageData as DMD
import qualified Messaging.TurboAvailableMessageData as TAMD
import qualified Messaging.LapFinishedMessageData as LFMD
import qualified Messaging.FinishMessageData as FMD
import qualified Messaging.CrashMessageData as CMD
import qualified Messaging.SpawnMessageData as SMD 
import qualified Messaging.GameEndMessageData as GEMD
import qualified Messaging.CarPositionsMessageData as CPMD
import qualified Messaging.GameInitMessageData as GIMD
import qualified Messaging.TurboStartMessageData as TSMD
import qualified Messaging.TurboEndMessageData as TEMD

data ServerMessage = ServerMessage {
    messageData :: ServerMessageData,
    gameId :: Maybe String,
    tick :: Maybe Int
}

data ServerMessageData = 
    Joined JMD.JoinedData
  | JoinedRace JRMD.JoinedRaceData
  | YourCar YCMD.YourCarData
  | GameInit GIMD.GameInitData 
  | CarPositions [CPMD.CarPositionsData]
  | Disqualified DMD.DisqualifiedData
  | GameStart
  | TurboAvailable TAMD.TurboAvailableData
  | TurboStart TSMD.TurboStartData
  | TurboEnd TEMD.TurboEndData
  | LapFinished LFMD.LapFinishedData
  | Finish FMD.FinishData
  | TournamentEnd
  | Crash CMD.CrashData
  | Spawn SMD.SpawnData
  | GameEnd GEMD.GameEndData
  | Unknown String String

instance Show ServerMessageData where
    show (Joined _) = "Joined"
    show (JoinedRace _) = "JoinedRace"
    show (YourCar _) = "YourCar"
    show (GameInit _) = "GameInit"
    show (CarPositions _ ) = "CarPositions"
    show (Disqualified _) = "Disqualified"
    show GameStart = "GameStart"
    show (TurboAvailable _) = "TurboAvailable"
    show (TurboStart _) = "TurboStart"
    show (TurboEnd _) = "TurboEnd"
    show (LapFinished _) = "LapFinished"
    show (Finish _) = "Finish"
    show TournamentEnd = "TournamentEnd"
    show (Crash _) = "Crash"
    show (Spawn _) = "Spawn"
    show (GameEnd _) = "GameEnd"
    show (Unknown _ _) = "Unknown"
  
toMessage :: ServerMessageContainer -> ServerMessage
toMessage container = ServerMessage {
    messageData = case decodeMessage (smcMessageType container, smcMessageData container) of
        Success message -> message
        Error s -> throw $ PatternMatchFail ("Error decoding message: " ++ s),
    gameId = smcGameId container,
    tick = smcTick container
} where
    decodeMessage ("join", msgData) = Joined <$> fromJSON msgData
    decodeMessage ("joinRace", msgData) = JoinedRace <$> fromJSON msgData
    decodeMessage ("yourCar", msgData) = YourCar <$> fromJSON msgData 
    decodeMessage ("gameInit", msgData) = GameInit <$> fromJSON msgData
    decodeMessage ("carPositions", msgData) = CarPositions <$> fromJSON msgData
    decodeMessage ("gameStart", _) = Success GameStart
    decodeMessage ("dnf", msgData) = Disqualified <$> fromJSON msgData
    decodeMessage ("turboAvailable", msgData) = TurboAvailable <$> fromJSON msgData
    decodeMessage ("turboStart", msgData) = TurboStart <$> TSMD.TurboStartData <$> fromJSON msgData
    decodeMessage ("turboEnd", msgData) = TurboEnd <$> TEMD.TurboEndData <$> fromJSON msgData
    decodeMessage ("lapFinished", msgData) = LapFinished <$> fromJSON msgData
    decodeMessage ("finish", msgData) = Finish <$> fromJSON msgData
    decodeMessage ("tournamentEnd", _) = Success TournamentEnd
    decodeMessage ("crash", msgData) = Crash <$> fromJSON msgData
    decodeMessage ("spawn", msgData) = Spawn <$> fromJSON msgData
    decodeMessage ("gameEnd", msgData) = GameEnd <$> fromJSON msgData
    decodeMessage (msgType, msgData) = Unknown msgType <$> fromJSON msgData


data ServerMessageContainer = ServerMessageContainer {
    smcMessageType :: String,
    smcMessageData :: Value,
    smcGameId :: Maybe String,
    smcTick :: Maybe Int
}

instance FromJSON ServerMessageContainer where
    parseJSON (Object v) =
        ServerMessageContainer <$>
        (v .: "msgType") <*>
        (v .: "data") <*>
        (v .:? "gameId") <*>
        (v .:? "tick")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object" 

