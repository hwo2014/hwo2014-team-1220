{-# LANGUAGE OverloadedStrings #-}

module Messaging.SpawnMessageData where


import Util

import qualified Model.CarId as CarId


type SpawnData = CarId.CarId

name :: SpawnData -> Name
name = CarId.name

color :: SpawnData -> Color
color = CarId.color
