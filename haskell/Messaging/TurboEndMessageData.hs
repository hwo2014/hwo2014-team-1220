{-# LANGUAGE OverloadedStrings #-}

module Messaging.TurboEndMessageData where


import Util

import qualified Model.CarId as CarId


data TurboEndData = TurboEndData {
    car :: CarId.CarId
}

instance Default TurboEndData where
    def = TurboEndData def
    
name :: TurboEndData -> Name
name = CarId.name . car

color :: TurboEndData -> Color
color = CarId.color . car
