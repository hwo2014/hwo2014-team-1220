{-# LANGUAGE OverloadedStrings #-}

module Messaging.CarPositionsMessageData where


import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))
import Data.List
import Data.Maybe

import Util

import qualified Model.CarId as CarId
import qualified Model.Position as Position
import qualified Model.LanePosition as LanePosition


data CarPositionsData = CarPositionsData {
  car      :: CarId.CarId,
  angle    :: Float,
  position :: Position.Position
} deriving (Show)

instance Default CarPositionsData where
    def = CarPositionsData def def def

instance FromJSON CarPositionsData where
    parseJSON (Object v) =
        CarPositionsData <$>
        (v .: "id") <*>
        (v .: "angle") <*>
        (v .: "piecePosition")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"


stephan :: [CarPositionsData] -> CarPositionsData
stephan = getCar "CaaS_Stephan"

mathias :: [CarPositionsData] -> CarPositionsData
mathias = getCar "CaaS_Mathias"

sebastian :: [CarPositionsData] -> CarPositionsData
sebastian = getCar "CaaS_Sebastian"
    
getCar :: Name -> [CarPositionsData] -> CarPositionsData
getCar carName = fromJust . findCar carName   
    
name :: CarPositionsData -> Name
name = CarId.name . car

color :: CarPositionsData -> Color
color = CarId.color . car

pieceIndex :: CarPositionsData -> Int
pieceIndex = Position.pieceIndex . position 

inPieceDistance :: CarPositionsData -> Float
inPieceDistance = Position.inPieceDistance . position

lanePosition :: CarPositionsData -> LanePosition.LanePosition
lanePosition = Position.lane . position

startIndex :: CarPositionsData -> Int
startIndex = LanePosition.startIndex . lanePosition

endIndex   :: CarPositionsData -> Int
endIndex = LanePosition.endIndex . lanePosition

lap :: CarPositionsData -> Int
lap = Position.lap . position

findCar :: String -> [CarPositionsData] -> Maybe CarPositionsData
findCar carName = find nameMatches where
    nameMatches carPosition = CarId.name (car carPosition) == carName




