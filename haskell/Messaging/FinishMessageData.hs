{-# LANGUAGE OverloadedStrings #-}

module Messaging.FinishMessageData where

import Util

import qualified Model.CarId as CarId


type FinishData = CarId.CarId

name :: FinishData -> Name
name = CarId.name

color :: FinishData -> Color
color = CarId.color