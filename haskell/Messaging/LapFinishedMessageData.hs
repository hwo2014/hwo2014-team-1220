{-# LANGUAGE OverloadedStrings #-}

module Messaging.LapFinishedMessageData where


import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.CarId as CarId
import qualified Model.LapTime as LapTime
import qualified Model.RaceTime as RaceTime
import qualified Model.Ranking as Ranking


data LapFinishedData = LapFinishedData {
    car :: CarId.CarId,
    lapTime :: LapTime.LapTime,
    raceTime :: RaceTime.RaceTime,
    ranking :: Ranking.Ranking
}

instance FromJSON LapFinishedData where
    parseJSON (Object v) =
        LapFinishedData <$>
        (v .: "car") <*>
        (v .: "lapTime") <*>
        (v .: "raceTime") <*>
        (v .: "ranking")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"


name :: LapFinishedData -> Name
name = CarId.name . car

color :: LapFinishedData -> Color
color = CarId.color . car

lap :: LapFinishedData -> Int
lap = LapTime.lap . lapTime

lapTicks :: LapFinishedData -> Int
lapTicks = LapTime.ticks . lapTime

lapMillis :: LapFinishedData -> Int
lapMillis = LapTime.millis . lapTime

laps :: LapFinishedData -> Int
laps = RaceTime.laps . raceTime

raceTicks :: LapFinishedData -> Int
raceTicks = RaceTime.ticks . raceTime

raceMillis :: LapFinishedData -> Int
raceMillis = RaceTime.millis . raceTime

overall :: LapFinishedData -> Int
overall = Ranking.overall . ranking

fastestLap :: LapFinishedData -> Int
fastestLap = Ranking.fastestLap . ranking
