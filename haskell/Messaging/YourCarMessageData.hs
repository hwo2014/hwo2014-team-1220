{-# LANGUAGE OverloadedStrings #-}

module Messaging.YourCarMessageData where


import Util

import qualified Model.CarId as CarId


type YourCarData = CarId.CarId


name :: YourCarData -> Name
name = CarId.name

color :: YourCarData -> Color
color = CarId.color