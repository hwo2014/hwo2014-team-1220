{-# LANGUAGE OverloadedStrings #-}

module Messaging.GameInitMessageData where


import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.Race as Race
import qualified Model.Track as Track
import qualified Model.StartingPoint as StartingPoint
import qualified Model.Point as Point
import qualified Model.Piece as Piece
import qualified Model.Lane as Lane
import qualified Model.Car as Car
import qualified Model.CarId as CarId
import qualified Model.CarDimensions as CarDimensions
import qualified Model.Session as Session
import qualified Model.Segment as Segment


data GameInitData = GameInitData {
  race :: Race.Race
} deriving (Show)

instance FromJSON GameInitData where
    parseJSON (Object v) = GameInitData <$> (v .: "race")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default GameInitData where
    def = GameInitData def
    
track :: GameInitData -> Track.Track
track = Race.track . race

trackName :: GameInitData -> Name
trackName = Track.name . track

startingPoint :: GameInitData -> StartingPoint.StartingPoint
startingPoint = Track.startingPoint . track

startingPosition :: GameInitData -> Point.Point
startingPosition = StartingPoint.position . startingPoint

startX :: GameInitData -> Float
startX = Point.x . startingPosition

startY :: GameInitData -> Float
startY = Point.y . startingPosition

startAngle :: GameInitData -> Float
startAngle = StartingPoint.angle . startingPoint

pieces :: GameInitData -> Piece.Pieces
pieces = Track.pieces . track

segments :: GameInitData -> Segment.Segments
segments = Track.segments . track

piece :: Int -> GameInitData -> Piece.Piece
piece index = (!! index) . pieces

pieceLength :: Int -> GameInitData -> Float
pieceLength index = Piece.length . (!! index) . pieces

pieceSwitch :: Int -> GameInitData -> Bool
pieceSwitch index = Piece.switch . (!! index) . pieces

pieceRadius :: Int -> GameInitData -> Int
pieceRadius index = Piece.radius . (!! index) . pieces

pieceAngle :: Int -> GameInitData -> Float
pieceAngle index = Piece.angle . (!! index) . pieces

pieceBridge :: Int -> GameInitData -> Bool
pieceBridge index = Piece.bridge . (!! index) . pieces

lanes :: GameInitData -> Lane.Lanes
lanes = Track.lanes . track

lane :: Int -> GameInitData -> Lane.Lane
lane index = (!! index) . lanes

laneCenterDistance :: Int -> GameInitData -> Int
laneCenterDistance index = Lane.centerDistance . (!! index) . lanes

laneIndex :: Int -> GameInitData -> Int
laneIndex index = Lane.index . (!! index) . lanes

cars :: GameInitData -> Car.Cars
cars = Race.cars . race 

car :: Int -> GameInitData -> Car.Car
car index = (!! index) . cars

carId :: Int -> GameInitData -> CarId.CarId
carId index = Car.carId . (!! index) . cars

carName :: Int -> GameInitData -> Name
carName index = CarId.name . Car.carId . (!! index) . cars

carColor :: Int -> GameInitData -> Color
carColor index = CarId.color . Car.carId . (!! index) . cars

carDimensions :: Int -> GameInitData -> CarDimensions.CarDimensions
carDimensions index = Car.dimensions . (!! index) . cars

carLength :: Int -> GameInitData -> Float
carLength index = CarDimensions.length . Car.dimensions . (!! index) . cars

carWidth :: Int -> GameInitData -> Float
carWidth index = CarDimensions.width . Car.dimensions . (!! index) . cars

carFlagPosition :: Int -> GameInitData -> Float
carFlagPosition index = CarDimensions.flagPosition . Car.dimensions . (!! index) . cars

session :: GameInitData -> Session.Session
session = Race.session . race

sessionLaps :: GameInitData -> Maybe Int
sessionLaps = Session.laps . session

sessionDuration :: GameInitData -> Maybe Int
sessionDuration = Session.duration . session

sessionMaxLapTime :: GameInitData -> Maybe Int
sessionMaxLapTime = Session.maxLapTime . session

sessionQuickRace :: GameInitData -> Maybe Bool
sessionQuickRace = Session.quickRace . session

