{-# LANGUAGE OverloadedStrings #-}

module Messaging.JoinedRaceMessageData where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.CarLogin as CarLogin

data JoinedRaceData = JoinedRaceData {
    car :: CarLogin.CarLogin,
    track :: Name,
    carCount :: Int,
    password :: Password
} deriving(Show)

instance FromJSON JoinedRaceData where
    parseJSON (Object v) =
        JoinedRaceData <$>
        (v .: "botId") <*>
        (v .: "trackName") <*>
        (v .: "carCount") <*>
        (v .: "password")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

