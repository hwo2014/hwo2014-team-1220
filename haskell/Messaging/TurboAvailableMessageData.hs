{-# LANGUAGE OverloadedStrings #-}

module Messaging.TurboAvailableMessageData where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))
import Util

data TurboAvailableData = TurboAvailableData {
    turboDuration :: Double,
    turboDurationTicks :: Int,
    turboFactor :: Double
}

instance FromJSON TurboAvailableData where
    parseJSON (Object v) =
        TurboAvailableData <$>
        (v .: "turboDurationMilliseconds") <*>
        (v .: "turboDurationTicks") <*>
        (v .: "turboFactor")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default TurboAvailableData where
    def = TurboAvailableData def def def
