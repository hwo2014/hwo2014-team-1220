{-# LANGUAGE OverloadedStrings #-}

module Messaging.GameEndMessageData where


import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.GameEndResult as GameEndResult
import qualified Model.GameEndBestLaps as GameEndBestLaps
import qualified Model.CarId as CarId
import qualified Model.RaceTime as RaceTime
import qualified Model.LapTime as LapTime


data GameEndData = GameEndData {
    results :: GameEndResult.GameEndResults,
    bestLaps :: GameEndBestLaps.GameEndBestLaps
}

instance FromJSON GameEndData where
    parseJSON (Object v) =
        GameEndData <$>
        (v .: "results") <*>
        (v .: "bestLaps")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"


resultCar :: Int -> GameEndData -> CarId.CarId
resultCar index = GameEndResult.car . (!! index) . results
  
resultName :: Int -> GameEndData -> Name
resultName index = CarId.name . GameEndResult.car . (!! index) . results

resultColor :: Int -> GameEndData -> Color
resultColor index = CarId.color . GameEndResult.car . (!! index) . results

resultRaceTime :: Int -> GameEndData -> RaceTime.RaceTime
resultRaceTime index = GameEndResult.result . (!! index) . results

resultLaps :: Int -> GameEndData -> Int
resultLaps index = RaceTime.laps . GameEndResult.result . (!! index) . results

resultTicks :: Int -> GameEndData -> Int
resultTicks index = RaceTime.ticks . GameEndResult.result . (!! index) . results

resultMillis :: Int -> GameEndData -> Int
resultMillis index = RaceTime.millis . GameEndResult.result . (!! index) . results

bestLapsCar  :: Int -> GameEndData -> CarId.CarId
bestLapsCar index = GameEndBestLaps.car . (!! index) . bestLaps

bestLapsName :: Int -> GameEndData -> Name
bestLapsName index = CarId.name . GameEndBestLaps.car . (!! index) . bestLaps

bestLapsColor :: Int -> GameEndData -> Color
bestLapsColor index = CarId.color . GameEndBestLaps.car . (!! index) . bestLaps

bestLapsLapTime :: Int -> GameEndData -> LapTime.LapTime
bestLapsLapTime index = GameEndBestLaps.result . (!! index) . bestLaps

bestLapsLap :: Int -> GameEndData -> Int
bestLapsLap index = LapTime.lap . GameEndBestLaps.result . (!! index) . bestLaps

bestLapsTicks :: Int -> GameEndData -> Int
bestLapsTicks index = LapTime.ticks . GameEndBestLaps.result . (!! index) . bestLaps

bestLapsMillis :: Int -> GameEndData -> Int
bestLapsMillis index = LapTime.millis . GameEndBestLaps.result . (!! index) . bestLaps


