{-# LANGUAGE OverloadedStrings #-}

module Messaging.ClientMessage where

import Data.List

import Util

data ClientMessage = 
    Join Name Key
  | Throttle Double
  | Ping
  | Switch Direction
  | Turbo 
  | CreateRace Name Key Name Password Count
  | JoinRace Name Key Name Password Count
  | JoinCIRace Name Key

instance Show ClientMessage where
    show (Join name key) = createMessage "join" $ createObject [("name", quote name), ("key", quote key)]
    show (Throttle amount) = createMessage "throttle" $ show amount
    show Ping = createMessage "ping" "{}"
    show (Switch direction) = createMessage "switchLane" $ quote (show direction)
    show Turbo = createMessage "turbo" $ quote "test"
    show (CreateRace carName carKey trackName password carCount) = createMessage "createRace" payload where
        payload = createObject [("botId", botId), ("trackName", quote trackName), ("password", quote password), ("carCount", show carCount)]
        botId = createObject [("name", quote carName), ("key", quote carKey)]
    show (JoinRace carName carKey trackName password carCount) = createMessage "joinRace" payload where
        payload = createObject [("botId", botId), ("trackName", quote trackName), ("password", quote password), ("carCount", show carCount)]
        botId = createObject [("name", quote carName), ("key", quote carKey)]
    show (JoinCIRace carName carKey) = createMessage "joinRace" payload where
        payload = createObject [("botId", botId)]
        botId = createObject [("name", quote carName), ("key", quote carKey)]

createMessage :: String -> String -> String
createMessage msgType payload = createObject [("msgType", quote msgType), ("data", payload)]

createObject :: [(String, String)] -> String
createObject = embrace . sep . map (uncurry keyValue)
    
embrace :: String -> String
embrace string = "{" ++ string ++ "}"

quote :: String -> String
quote string = "\"" ++ string ++ "\""

keyValue :: String -> String -> String
keyValue key value = quote key ++ ":" ++ value

sep :: [String] -> String
sep = intercalate ","