module State.State where

    
import Sebastian.Model.Model as Model

import qualified Messaging.GameInitMessageData as GIMD
import qualified Messaging.CarPositionsMessageData as CPMD
import qualified Messaging.TurboAvailableMessageData as TAMD
import qualified Model.Segment as Segment

import Mathias.MathiasState(MathiasState())
import Util


data BotState = BotState {
    gimd :: GIMD.GameInitData,
    cpmd :: [CPMD.CarPositionsData],
    tamd :: Maybe TAMD.TurboAvailableData,
    botName :: Name,
    individualState :: IndividualState
}

instance Show BotState where
    show = (++) "BotName" . botName

data IndividualState =
      MaState MathiasState
    | SeState SebastiansBotState
    | StState StephanState
    | DeState
    
instance Default IndividualState where def = DeState

instance Default BotState where
    def = BotState def def def def def

data SebastiansBotState = SebastiansBotState {
    randoms     :: [Double],
    environment :: Model.Model
}

instance Default SebastiansBotState where
    def = SebastiansBotState [] def

data StephanState = StephanState {
    pieceIndex :: Int
}

instance Default StephanState where
    def = StephanState 0


currentSegment :: BotState -> Segment.Segment
currentSegment state = Segment.segmentAt currentIndex segments where
        currentIndex = CPMD.pieceIndex $ CPMD.getCar (botName state) (cpmd state)
        segments = GIMD.segments $ gimd state

ownCar :: Name -> BotState -> Bool
ownCar name = (==) name . botName
