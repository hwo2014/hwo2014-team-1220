module Model.Segment where

import Control.Exception
import Data.List
import Data.Maybe
import Util


data Segment = Segment {
    length :: Length,
    segmentType :: SegmentType,
    startIndex :: Index,
    endIndex :: Index
} deriving (Show)

data SegmentType =
    Straight 
  | Curve { radius :: Radius, direction :: Direction }
  deriving (Show) 
  
instance Eq SegmentType where
    (==) Straight Straight = True
    (==) (Curve r1 d1) (Curve r2 d2) = r1 == r2 && d1 == d2
    (==) _ _ = False

type Segments = [Segment]

straight :: Segment -> Bool
straight = (==) Straight . segmentType

curve :: Segment -> Bool
curve segment = case segmentType segment of
    Straight -> False
    Curve _ _ -> True 

segmentAt :: Index -> Segments -> Segment
segmentAt pieceIndex segments = fromMaybe throwFunc $ safeSegmentAt pieceIndex segments where
    throwFunc = throw $ PatternMatchFail ("Index " ++ show pieceIndex ++ " out of track")
        
safeSegmentAt :: Index -> Segments -> Maybe Segment
safeSegmentAt pieceIndex = find (indexBetweenBounds pieceIndex) where
    indexBetweenBounds i s = i >= startIndex s && i <= endIndex s