{-# LANGUAGE OverloadedStrings #-}

module Model.LanePosition where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

data LanePosition = LanePosition {
    startIndex :: Int,
    endIndex   :: Int
} deriving (Show)

instance FromJSON LanePosition where
    parseJSON (Object v) =
        LanePosition <$>
        (v .: "startLaneIndex") <*>
        (v .: "endLaneIndex")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"
    
instance Default LanePosition where def = LanePosition def def
