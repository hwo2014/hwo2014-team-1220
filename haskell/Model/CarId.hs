{-# LANGUAGE OverloadedStrings #-}

module Model.CarId where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

data CarId = CarId {
    name :: Name,
    color :: Color
} deriving (Show)

instance Default CarId where def = CarId def def

instance FromJSON CarId where
    parseJSON (Object v) = CarId <$> (v .: "name") <*> (v .: "color")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"