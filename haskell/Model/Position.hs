{-# LANGUAGE OverloadedStrings #-}

module Model.Position where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.LanePosition as LanePosition

data Position = Position {
  pieceIndex      :: Int,
  inPieceDistance :: Float,
  lane            :: LanePosition.LanePosition,
  lap             :: Int
} deriving (Show)

instance FromJSON Position where
    parseJSON (Object v) =
        Position <$>
        (v .: "pieceIndex")      <*>
        (v .: "inPieceDistance") <*>
        (v .: "lane")            <*>
        (v .: "lap")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default Position where def = Position def def def def
