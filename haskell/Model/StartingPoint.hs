{-# LANGUAGE OverloadedStrings #-}

module Model.StartingPoint where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.Point as Point


data StartingPoint = StartingPoint {
    position :: Point.Point,
    angle    :: Float
} deriving (Show)

instance FromJSON StartingPoint where
    parseJSON (Object v) =
        StartingPoint <$>
        (v .: "position") <*>
        (v .: "angle")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default StartingPoint where
    def = StartingPoint def def
