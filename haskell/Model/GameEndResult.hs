{-# LANGUAGE OverloadedStrings #-}

module Model.GameEndResult where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import qualified Model.CarId as CarId
import qualified Model.RaceTime as RaceTime

data GameEndResult = GameEndResult {
    car :: CarId.CarId,
    result :: RaceTime.RaceTime
} deriving (Show)    

instance FromJSON GameEndResult where
    parseJSON (Object v) =
        GameEndResult <$>
        (v .: "car") <*>
        (v .: "result")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

type GameEndResults = [GameEndResult]
