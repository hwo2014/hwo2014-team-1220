{-# LANGUAGE OverloadedStrings #-}

module Model.Track where


import Control.Applicative ((<$>), (<*>))
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))
import qualified Data.Vector as Vector

import Util

import qualified Model.StartingPoint as StartingPoint
import qualified Model.Piece as Piece
import qualified Model.Lane as Lane 
import qualified Model.Segment as Segment


data Track = Track {
    name          :: Name,
    startingPoint :: StartingPoint.StartingPoint,
    pieces        :: Piece.Pieces,
    lanes         :: Lane.Lanes
} deriving (Show)

instance FromJSON Track where
    parseJSON (Object v) =
        Track <$>
        (v .: "name") <*>
        (v .: "startingPoint") <*>
        (v .: "pieces") <*>
        (v .: "lanes")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default Track where
    def = Track def def def def


trackLength :: Track -> Length
trackLength = combinedLength . pieces

combinedLength :: Piece.Pieces -> Length
combinedLength = sum . map Piece.length

currentTrackPosition :: Int -> Float -> Track -> Float
currentTrackPosition pieceIndex inPiecePosition track = currentLength / trackLength track where
    currentLength = (combinedLength . take pieceIndex $ pieces track) + inPiecePosition

segments :: Track -> Segment.Segments
segments = convertPieces . Vector.fromList . pieces where
    convertPieces ps = if Vector.null ps 
        then []
        else Vector.ifoldr convertNext [] ps where
        convertNext index piece [] = [newSegment piece index]
        convertNext index piece segs@(x:xs) 
            | haveDifferentSegmentType piece x = newSegment piece index : segs
            | otherwise                        = modifySegment x piece index : xs
        haveDifferentSegmentType p s = segmentType p /= Segment.segmentType s
        modifySegment segment piece index = segment {
            Segment.length = Segment.length segment + Piece.length piece,
            Segment.startIndex = index
        }
        newSegment piece index = Segment.Segment {
            Segment.length = Piece.length piece,
            Segment.segmentType = segmentType piece,
            Segment.startIndex = index,
            Segment.endIndex = index
        }
        segmentType piece = if Piece.straight piece 
            then Segment.Straight 
            else Segment.Curve (Piece.radius piece) (Piece.direction piece)
        
piecesOf :: Segment.Segment -> Track -> Piece.Pieces
piecesOf segment = take numberToTake . drop numberToDrop . pieces where
    numberToDrop = Segment.startIndex segment
    numberToTake = Segment.endIndex segment - Segment.startIndex segment + 1
