{-# LANGUAGE OverloadedStrings #-}

module Model.Lane where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

data Lane = Lane {
    centerDistance :: Int,
    index          :: Int
} deriving (Show)

instance FromJSON Lane where
    parseJSON (Object v) =
        Lane <$>
        (v .: "distanceFromCenter") <*>
        (v .: "index")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

type Lanes = [Lane]
