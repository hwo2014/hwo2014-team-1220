{-# LANGUAGE OverloadedStrings #-}

module Model.Session where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:?))

import Util


data Session = Session {
    laps       :: Maybe Int,
    duration   :: Maybe Int,
    maxLapTime :: Maybe Int,
    quickRace  :: Maybe Bool
} deriving (Show)

instance FromJSON Session where
    parseJSON (Object v) =
        Session <$>
        (v .:? "laps") <*>
        (v .:? "durationMs") <*>
        (v .:? "maxLapTimeMs") <*>
        (v .:? "quickRace")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default Session where
    def = Session def def def def
