{-# LANGUAGE OverloadedStrings #-}

module Model.RaceTime where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:?))
import Data.Maybe


data RaceTime = RaceTime {
    rawLaps :: Maybe Int,
    rawTicks :: Maybe Int,
    rawMillis :: Maybe Int
} deriving (Show)

instance FromJSON RaceTime where
    parseJSON (Object v) = RaceTime <$> (v .:? "laps") <*> (v .:? "ticks") <*> (v .:? "millis")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

    
laps :: RaceTime -> Int    
laps = fromMaybe 0 . rawLaps

ticks :: RaceTime -> Int
ticks = fromMaybe 0 . rawTicks

millis :: RaceTime -> Int
millis = fromMaybe 0 . rawMillis



