{-# LANGUAGE OverloadedStrings #-}

module Model.Piece where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:?))
import Data.Maybe

import Util


data Piece = Piece {
    rawLength :: Maybe Length,
    rawSwitch :: Maybe Bool,
    rawRadius :: Maybe Radius,
    rawAngle  :: Maybe Angle,
    rawBridge :: Maybe Bool
} 

instance FromJSON Piece where
    parseJSON (Object v) =
        Piece <$>
        (v .:? "length") <*>
        (v .:? "switch") <*>
        (v .:? "radius") <*>
        (v .:? "angle")  <*>
        (v .:? "bridge")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Show Piece where
    show piece = concat ["[Length: ", show l, " Radius: ", show r, " Angle: " , show a, " Switch: ", show s, "]"] where
        l = Model.Piece.length piece
        r = Model.Piece.radius piece
        a = Model.Piece.angle piece
        s = Model.Piece.switch piece

instance Default Piece where
    def = Piece def def def def def
    
type Pieces = [Piece]
    
length :: Piece -> Float
length piece = if straight piece then piecelength piece else calculateLength where
    calculateLength = abs $ pi * fromIntegral (radius piece) * (angle piece / 180.0)
    piecelength = fromMaybe 0 . rawLength

switch :: Piece -> Bool
switch = fromMaybe False . rawSwitch

radius :: Piece -> Int
radius = fromMaybe 0 . rawRadius

angle :: Piece -> Float
angle = fromMaybe 0 . rawAngle

bridge :: Piece -> Bool
bridge = fromMaybe False . rawBridge

straight :: Piece -> Bool
straight = (== 0) . angle

curve :: Piece -> Bool
curve = not . straight

isLeftCurve :: Piece -> Bool
isLeftCurve = (< 0) . angle

isRightCurve :: Piece -> Bool
isRightCurve = (> 0) . angle

direction :: Piece -> Direction
direction piece | isLeftCurve piece = ToLeft
                | isRightCurve piece = ToRight
                | otherwise = throw $ PatternMatchFail "Not a curve - check first with Piece.curve"

