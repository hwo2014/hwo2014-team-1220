{-# LANGUAGE OverloadedStrings #-}

module Model.LapTime where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:?))
import Data.Maybe


data LapTime = LapTime {
    rawLap :: Maybe Int,
    rawTicks :: Maybe Int,
    rawMillis :: Maybe Int
} deriving (Show)

instance FromJSON LapTime where
    parseJSON (Object v) = LapTime <$> (v .:? "lap") <*> (v .:? "ticks") <*> (v .:? "millis")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"
    

lap :: LapTime -> Int    
lap = fromMaybe 0 . rawLap

ticks :: LapTime -> Int
ticks = fromMaybe 0 . rawTicks

millis :: LapTime -> Int
millis = fromMaybe 0 . rawMillis