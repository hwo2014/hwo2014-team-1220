{-# LANGUAGE OverloadedStrings #-}

module Model.Race where

    
import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

import qualified Model.Track as Track
import qualified Model.Car as Car
import qualified Model.Session as Session


data Race = Race {
    track   :: Track.Track,
    cars    :: Car.Cars,
    session :: Session.Session
} deriving (Show)

instance FromJSON Race where
    parseJSON (Object v) =
        Race <$>
        (v .: "track") <*>
        (v .: "cars") <*>
        (v .: "raceSession")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default Race where
    def = Race def def def
