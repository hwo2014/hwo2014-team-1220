{-# LANGUAGE OverloadedStrings #-}

module Model.GameEndBestLaps where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import qualified Model.CarId as CarId
import qualified Model.LapTime as LapTime

data GameEndBestLap = GameEndBestLap {
    car :: CarId.CarId,
    result :: LapTime.LapTime
} deriving (Show)    

instance FromJSON GameEndBestLap where
    parseJSON (Object v) =
        GameEndBestLap <$>
        (v .: "car") <*>
        (v .: "result")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

type GameEndBestLaps = [GameEndBestLap]
