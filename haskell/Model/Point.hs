{-# LANGUAGE OverloadedStrings #-}

module Model.Point where


import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util


data Point = Point {
    x :: Float,
    y :: Float
} deriving (Show)

instance FromJSON Point where
    parseJSON (Object v) = Point <$> (v .: "x") <*> (v .: "y")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

instance Default Point where
    def = Point def def
