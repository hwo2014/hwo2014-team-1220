{-# LANGUAGE OverloadedStrings #-}

module Model.CarDimensions where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

data CarDimensions = CarDimensions {
    length       :: Float,
    width        :: Float,
    flagPosition :: Float
} deriving (Show)

instance FromJSON CarDimensions where
    parseJSON (Object v) =
        CarDimensions <$>
        (v .: "length") <*>
        (v .: "width") <*>
        (v .: "guideFlagPosition")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"
