{-# LANGUAGE OverloadedStrings #-}

module Model.Car where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import qualified Model.CarId as CarId
import qualified Model.CarDimensions as CarDimensions

data Car = Car {
  carId      :: CarId.CarId,
  dimensions :: CarDimensions.CarDimensions
} deriving (Show)

instance FromJSON Car where
    parseJSON (Object v) =
        Car <$>
        (v .: "id") <*>
        (v .: "dimensions")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

type Cars = [Car]
