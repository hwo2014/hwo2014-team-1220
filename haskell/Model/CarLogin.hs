{-# LANGUAGE OverloadedStrings #-}

module Model.CarLogin where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

import Util

data CarLogin = CarLogin {
    name :: Name,
    key :: Key
} deriving (Show)

instance FromJSON CarLogin where
    parseJSON (Object v) = CarLogin <$> (v .: "name") <*> (v .: "key")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"
