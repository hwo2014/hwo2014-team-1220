{-# LANGUAGE OverloadedStrings #-}

module Model.Ranking where

import Control.Applicative
import Control.Exception
import Data.Aeson (FromJSON (..), Value (..), parseJSON, (.:))

data Ranking = Ranking {
    overall :: Int,
    fastestLap :: Int
} deriving (Show)

instance FromJSON Ranking where
    parseJSON (Object v) = Ranking <$> (v .: "overall") <*> (v .: "fastestLap")
    parseJSON _ = throw $ PatternMatchFail "Parse error: Not a JSON object"

