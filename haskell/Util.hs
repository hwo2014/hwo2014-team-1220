module Util where


data Direction = ToLeft | ToRight deriving(Eq)

instance Show Direction where 
    show ToLeft = "Left"
    show ToRight = "Right"

type Name = String

type Names = [Name]

type Key = String

type Color = String

type Password = String

type Count = Int

type Length = Float

type Angle = Float

type Radius = Int

type Index = Int


class Default a where
    def :: a

instance Default [a] where
    def = []

instance Default Float where
    def = 0.0

instance Default Double where
    def = 0.0
    
instance Default Int where
    def = 0

instance Default (Maybe a) where
    def = Nothing
